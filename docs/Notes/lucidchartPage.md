# Lucidchart Notes

Various flowcharts and diagrams for the project.

!!! info ""
    - For best viewing, set the zoom to "Zoom to Page".
    - `Alt` + scroll to zoom.


<div style="width: 880px; height: 960px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:880px; height:960px" src="https://lucid.app/documents/embeddedchart/0bba310c-cedb-4734-8456-c2e9b96b54e3" id="k6dR8RNJo_kP"></iframe></div>
