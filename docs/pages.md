Things that I want to include in the software docs


1. Full overview of Ricochet
   1. What it is
   2. Scientific motivation
      1. To pursue research in detector technologies, like superconducting
      bolometers, specifically Ge and Zn. (see "Detector Technologies")
      2. Searching for new physics / non standard interactions, like
         1. neutrinos, explore posibility of sterile neutrino (expand)
   3. Detector Technologies
       1. Ge + Zn bolometers
       2. TES chips
       3. Multiplexing
2. Simulating the Detector Response
    1. Ultimate goal is to have a code framework that takes as input Geant4 sims (from ricochetsim output) and outputs a waveform.
    2. Model: POD approach
    3. What I've started with
       1. Pulse Modeling (own page)

3. Software Docs
   - This part will probably be more messy as features are developed
   - Simulation Toolchain
   - Ricochetsim
     - Summary, link to gitlab, developed by French collaboration members
     - Ricochetsim Output file format
   - RDER
     - Summary
       - Is meant to implement the model in section 2.
     - Project members
       - Classes (see this for reference)
         - RDER: Main class
         - MCTruthEvent: Corresponds to each event, designated by an eventID. Contains MCTruthPulse's and MCTruthVertex's
         - MCTruthPulse: Contains a collection of vertices within a time window (?)
         - MCTruthVertex: Contain variables that fully describe the physics during the simulated event.
