# Ricochet Simulation Output Library

## Output Description

The simulation output is a ROOT file which contains general information (generator, geometry, etc...) and the information for each simulated event. Information about each event is saved in a ROOT tree (`TTree`), each entry represents an event represented by the `SimuEvent` class/structure.

### SimuEvent class
- `std::vector<PrimaryEvent> PrimaryEvents`: list of primary particles generated in the current (Geant4) simulated event
- `std::vector<DetectorEvent> DetectorEvents`: list of detector events in the current (Geant4) simulated event, one detector event corresponds to a non-zero deposited energy in one bolometer

### PrimaryEvent class
- `std::string VolumeName`: volume name where the primary particle is generated
- `std::string MaterialName`: material name of the volume where the particle is generated
- `int PDG`: PDG ID of the particle, see `ParticleName()` function to get the name of the particle (see [PDG ref.](http://pdg.lbl.gov/2019/reviews/rpp2019-rev-monte-carlo-numbering.pdf))
- `double Time`: time when the primary particle was generated, relative to the beginning of the Geant4 simulated event (not the absolute time since the beginning of the simulation)
- `Geometry::Point<float> Position`: position where the primary particle is generated, in the laboratory frame
- `double E`: Initial kinetic energy of the primary particle
- `double Px`: Initial momentum along the X-axis of the primary particle
- `double Py`: Initial momentum along the Y-axis of the primary particle
- `double Pz`: Initial momentum along the Z-axis of the primary particle

- `std::string ParticleName() const`: function which returns the name of the primary particle, based on its PDG ID

### DetectorEvent class
- `unsigned int DetectorID`: unique ID of the current bolometer
- `std::string DetectorName`: name of the volume which represents the current bolometer
- `std::vector<HitEvent> HitEvents`: list of all the hits in the current bolometer for the current simulated event

- `double Energy() const`: total energy depostited in the current bolometer

### HitEvent class
- `double Edep`: energy deposited by the current hit
- `double Time`: time of the current hit since the begining of Geant4 simulated event
- `int PDG`: PDG ID of the incident particle which has produced the current hit (see [PDG ref.](http://pdg.lbl.gov/2019/reviews/rpp2019-rev-monte-carlo-numbering.pdf))
- `double IncidentEnergy`: incident particle energy
- `Geometry::Point<double> Position`: coordinates in the laboratory frame of the current hit vertex
- `Geometry::Point<double> LocalPosition`: local coordinates in the volume of the curent hit vertex
- `std::string ProcessName`: process name of the currrent hit vertex

- `std::string ParticleName() const`: function which returns the name of the incident particle, based on its PDG ID
