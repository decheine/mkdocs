# Ricochetsim

The simulation code of ricochet.

IPNL Gitlab: https://git.ipnl.in2p3.fr/ricochet/ricochetsim

- Summary, link to gitlab, developed by French collaboration members
# Simulated Output File Format
See [this](simuOutputFormat.md)
