# Simulation Software Documentation
This part will probably be more messy as features are developed
## Simulation Toolchain
//TODO: expand this list

This toolchain is comprised of a sequential series of modules that data is
put through for processing.

Simulated energy -> SimuEvents -> waveform construction -> multiplexing ->
electronics effects -> \* -> Readout
## Ricochetsim
### Summary
link to gitlab, developed by French collaboration members
   GitLab: [ricochetsim](https://git.ipnl.in2p3.fr/ricochet/ricochetsim)
### Ricochetsim Output file format
\*this is where the lucidchart document will go\*
## RDER
### Summary
Is meant to implement the model in section 2.

   - Project members
     - Classes (see this for reference)
       - RDER: Main class
       - Bolometer: Detector specific class that handles the detector perspective through the simulated run.
       - MCTruthEvent: Corresponds to each event, designated by an eventID. Contains MCTruthPulse's and MCTruthVertex's
       - MCTruthPulse: Contains a collection of vertices within a time window (?)
       - MCTruthVertex: Contain variables that fully describe the physics during the simulated event.
