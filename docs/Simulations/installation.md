# Installing RDER

1. Complete [setting up the development environment](https://decheine.gitlab.io/mkdocs/Resources/DevelopmentEnvironment/)
1. Set up folders and clone the repository

```bash
mkdir build install
git clone https://gitlab.com/decheine/rder.git
```

1. Configure

    
