# Simulating the Detector Response

1. Ultimate goal is to have a code framework that takes as input Geant4 sims (from ricochetsim output) and outputs a waveform.


2. Model: POD approach


3. What I've started with


   1. Pulse Modeling (own page)


4. Current model has the waveform be an array of length equal to the total event simulated time divided by some $dt$, holding voltage numbers. Each DetectorEvent will have a corresponding waveform object. Many of these would then be multiplexed by the multiplexer module
