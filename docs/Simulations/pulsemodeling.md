

# Pulse Modeling

## Preface
I preface this section with a review regarding theh structure of the ricochetsim output. A SimuEvents TTree contains $N_s$ simulated events, instances of `SimuEvent`, the number corresponding to the count specified in the parent macro. Each `SimuEvent` contains a list of `DetectorEvent`s, and each `DetectorEvent` contains a list of `HitEvents`. See the [class diagram](../Notes/lucidchartPage.md) for more details.


## Main Idea
!!! info "Main Idea"
    Each `SimuEvent` contains a collection of pulses that describe the detector response to that event, represented by the `Pulse` class.







## `Pulse`

A class for representing pulse data from a simulated energy deposit.

!!! info "Waveform visualization"
    A method of displaying waveforms take the form of a TF1 object, and as a double exponential, of the form:
    ```
    [0] + [1] * TMath::Exp(-x/[2]) + [3] * TMath::Exp(-x/[4])
    ```
    HOWEVER
    I've found a better way to do this, via sampling?



## `Bolometer`
A class representing a single bolometer. It is used to store data from runs that is relevant on a bolometer to bolometer basis. The class holds a bolometer's pulses, name, id, and more. (TODO: list these when finalized)


FindResponse Method that takes in Edep, PDG, and a later, more involved implementation can be used to generate a suitable detector response.


## Pulse Collection
Each DetectorEvent has an associated collection of pulses. These are recorded during iteration of the `runSim` method in the main source file, RDERsim.cpp.


## User-Adjustable Parameters

Variable | Description
--|--
`clusterTimeWindow` | Designates the time seperation that `HitEvent`'s must be from an existing pulse in a detector to count as a _different pulse_. Defaults to 0.1.
 `energyThreshold` | Designates a threshold energy `HiteEvent`'s must exceed to generate a pulse.
  |
  |
  |
  |
  |
  |
  |


**For DetectorVertexMCTruth**:
The DER copies the vertex information `DetectorVertexMCTruth`
