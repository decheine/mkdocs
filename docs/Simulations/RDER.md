# RDER
Main class of the software. Calls all the functional parts. Handles IO.
Is meant to implement the model described in [Simulating the Detector  Response](SimulatingDER.md)

# Project Members

## Classes (see this for reference)
### RDER
Main class
### MCTruthEvent
Corresponds to each event, designated by an eventID. Contains MCTruthPulse's and MCTruthVertex's
### MCTruthPulse
Contains a collection of vertices within a time window (?)
### MCTruthVertex
Contains variables that fully describe the physics during the simulated event. (?)
I'll read more in to this, [here](https://gitlab.com/luxzeplin/lzap/RawDataModel/-/tree/master/src/lib)
