
### Derived Classes

Making two dervied classes,

```
#ifndef CPPFACTORY_DERIVEDCLASSONE_H
#define CPPFACTORY_DERIVEDCLASSONE_H

#include "MyBaseClass.h"
#include <iostream>
using namespace std;

class DerivedClassOne : public MyBaseClass
{
public:
    DerivedClassOne(){};
    virtual ~DerivedClassOne(){};

    virtual void doSomething() { cout << "I am class one" << endl; }
};

#endif // CPPFACTORY_DERIVEDCLASSONE_H
```

and


```
#ifndef CPPFACTORY_DERIVEDCLASSTWO_H
#define CPPFACTORY_DERIVEDCLASSTWO_H

#include "MyBaseClass.h"
#include <iostream>
using namespace std;

class DerivedClassTwo : public MyBaseClass
{
public:
    DerivedClassTwo(){};
    virtual ~DerivedClassTwo(){};

    virtual void doSomething() { cout << "I am class two" << endl; }
};

#endif // CPPFACTORY_DERIVEDCLASSTWO_H
```





```
#include "MyFactorySimple.h"

#include "DerivedClassOne.h"
#include "DerivedClassTwo.h"

shared_ptr<MyBaseClass> MyFactory::CreateInstance(string name)
{
    MyBaseClass * instance = nullptr;

    if(name == "one")
        instance = new DerivedClassOne();

    if(name == "two")
        instance = new DerivedClassTwo();

    if(instance != nullptr)
        return std::shared_ptr<MyBaseClass>(instance);
    else
        return nullptr;
}
```
