# Ricochet Software Docs

This website, hosted through continuous deployment with GitLab pages, will serve as a readily accessible location for Ricochet software documentation. This site was initially created for an independent study in Fall 2020 by Nick Decheine.

Every time a new commit is pushed to `master`, CI/CD rebuilds the website. Note: it can take up to 30 minutes from commit time for the changes to be public.

## Project Structure Overview
- Ricochet Experiment
   - Scientific motivation
      - To pursue research in detector technologies, like superconducting
      bolometers, specifically Ge and Zn. (see "Detector Technologies")
      - Searching for new physics / non standard interactions, like
         - neutrinos, explore possibility of sterile neutrino (expand)
   - Detector Technologies
       - Ge + Zn bolometers
       - TES chips
       - Multiplexing
- Simulating the Detector Response
    - This part will probably be more messy as features are developed
    - Ultimate goal is to have a code framework that takes as input Geant4 sims (from ricochetsim output) and outputs a waveform representing a simulated .
    - Model: POD approach
    - What I've started with
       - Pulse Modeling (own page)
    - Simulation Toolchain
    - Ricochetsim
        - Summary, link to gitlab, developed by French collaboration members
        - Ricochetsim Output file format
    - RDER
        - Summary
        - Is meant to implement the model in section 2.
    - Project members
        - Classes (see this for reference)
            - RDER: Main class
            - MCTruthEvent: Corresponds to each event, designated by an eventID. Contains MCTruthPulse's and MCTruthVertex's
            - MCTruthPulse: Contains a collection of vertices within a time window (?)
            - MCTruthVertex: Contain variables that fully describe the physics during the simulated event.
