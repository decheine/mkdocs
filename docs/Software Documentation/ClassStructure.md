


### rootcling
Using `rootcling` to generate dictionaries. The way it worked for me is

``` bash
 rootcling -I=/home/nick/Ricochet/install/include eventdict.cxx -c ../include/Bolometer.h
```
The include flag has to be there so that the compiler knows where to find the SimuOutput headers. `evectdict.cxx` is the generated dictionary.
