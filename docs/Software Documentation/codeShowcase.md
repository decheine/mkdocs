# Code Showcase


List of files that I want to share

## RDERsim.cpp
Main source file that runs the software, constructs all the classes, and calls all the methods.

Methods:

### ConstructBolometers()

What this method does is

- Fetches a list of all the detector names that appear in the SimuOutput ROOT file.
    - Puts these in a Map with their DetectorID's
- Input: Simulated output filename
- Output: A mapping of all DetectorID's that appeared in the simulation to respective `Bolometer `objects

```c++
std::map<unsigned int, Bolometer> ConstructBolometers(std::string filename){
    std::map<std::string, unsigned int> detectorNames = readDetectorNames(filename);
    std::cout << "constructing bolos\n";

    // Map of bolometers
    std::map<unsigned int, Bolometer> bolometers;
    Bolometer tmpBolo;
    for(const auto& d : detectorNames){
        // std::cout << d.first << " ID: " << d.second << std::endl;
        tmpBolo = Bolometer(d.second, d.first);
        bolometers.insert(std::pair<unsigned int, Bolometer>(d.second,tmpBolo));
    }
    return bolometers;
}
```



### runSim()

This is where Truth information is handled and stored. Additional Truth information may be added by accessing PrimaryEvent's of the SimuEvent TTree

- Run through root file
    - When there's an energy deposit, make a new pulse for the detector the DetectorEvent took place in. That Pulse is then added to the Bolometer object corresponding to that specific detector.
- ...




# Classes

Primary, finished, and built classes integral to the RDER software:

- Bolometer
- Pulse

## `Pulse`

The following is a table of members belonging to the custom `Pulse` class

Type  | Member| Description
--|--|--
 TF1 |`waveform`| ROOT Function class that constructs a double exponential curve based on the parameters given in `parameters`
 vector\<double\> |`parameters`| Parameters for the TF1 waveform.\*
 double |startTime| corresponds to HitEvent.Time of the parent HitEvent.
 double |endTime| will later be used to designate the time frame in which the pulse dips below the trigger threshold.
 double |iEnergyDeposit| Deposited energy from it's HitEvent's
 unsigned int |Intensity| Intensity is iterated whenever there would be a hitevent that would make a new pulse.
 int |PDG| PDG code corresponding to the parent particle interaction.
 Geometry::Point\<double\> |globalPosition| Parent HitEvent's globalPosition
 Geometry::Point\<double\> |localPosition| Parent HitEvent's local position

\* these might not last in later iterations as more practical approaches replace them.

## `Bolometer`

The following is a table of members belonging to the custom `Bolometer` class

| Type                    | Member         | Description                                                  |
| ----------------------- | -------------- | ------------------------------------------------------------ |
| vector\<Pulse\>         | Pulses         | A vector of Pulse's that this bolometer experienced throughout the run. |
| vector\<DetectorEvent\> | detectorEvents | A vector of `DetectorEvent`s that this bolometer experienced throughout the run. |
| unsigned int            | detectorID     | Detector ID                                                  |
| String                  | detectorName   | Detector Name                                                |

snippets to for sure include in later iterations are:

the method doAnalyticResponse(): This function builds the analytic response using all the Pulses that the Bolometer object contains. This way



# Scaffolding

I decided to go the route of having fewer classes/structs in the begging in favour of functional, working ones. The way that the original Pulse and Bolometer classes are structured is such that pieces of the classes will be offloaded over time, as the greater implementation is constructed and those features moved into another class. (One reason for this was I would have to build and link each one into a dictionary, and I didn't want to waste too much time doing that.)



### `ConstructBasePulse(N)`

This is a method for constructing the base pulse for a properly sampled waveform.


```c++
double fDecayTime; double fRiseTime;
// Step Function
std::vector<double> fStep(N, 0);
for (int j = 0; j <= N * 0.5; j++){
    fStep[j] = 1.0;
}

fPMTPulseSamples.resize(N, 0);
std::array<double, 2> accus = { 0, 0 };
double samplingInterval = 1.0; 	// this is governed by precision info
std::array<double, 4> coeffs;	// Correspond to the parameters member of Pulse
fRiseTime = 3.81; 	// time constant rising - from fit (?) THIS COULD VARY
fDecayTime = 4.76; 	// time constant falling - from fit (?) THIS COULD VARY

coeffs[0] = exp(-samplingInterval /fDecayTime);
coeffs[1] = 1 - coeffs[0];
coeffs[2] = exp(-samplingInterval / fRiseTime);
coeffs[3] = 1 - coeffs[2];

Double_t index[N];
Double_t y[N];

for (int i = 0; i < N; i++){
    index[i] = i;
    accus[0] = (coeffs[0] * accus[0] - coeffs[1] * fStep[i]);
    accus[1] = (coeffs[2] * accus[1] - coeffs[3] * fStep[i]);
    fPMTPulseSamples[i] = accus[0] - accus[1];
    if (fPMTPulseSamples[i] > 0)
        fPMTPulseSamples[i] = 0;
    // std::cout << fPMTPulseSamples[i] << ", ";
    y[i] = fPMTPulseSamples[i];
}
///std::cout << "]\n";

/// ROOT Session
/// PLOT THE SAMPLED WAVEFORM
RunRootApp([&] (){
               TGraph *gr = new TGraph(N,index,y);
               TCanvas *c1 = new TCanvas("c1","Graph Draw Options",
                                         200,10,600,400);
               gr->Draw("");
               // drawing
           });
std::cout << "drawn\n";
```

### MCTruth Structs
Scaffolding classes and headers to pave the way for future development and implementations:

- MCTruthEvent
    - Corresponds to one SimuEvent, contains the other structs.

```c++
struct MCTruthEvent {
    std::vector<MCTruthPulse>    pulses;
    std::vector<MCTruthVertex>   vertices;
    TVector3    fParentPosition;
    TVector3    fParentDirection;
    double      fParentTime;
    float       fParentEnergy;
    Int_t       iSimuEventNumber;   // Corresponding SimuEvent
    std::string sParentParticleName;
    std::string sParentVolumeName;
}
```
- MCTruthPulse
    - A Pulse corresponds to one or more DetectorEvent's
```c++
class MCTruthPulse {
    short   pulseID;
    int     iVertexNumber; // corresponding vertex
    Pulse   pulse;  // Corresponding proper Pulse

};
```

- MCTruthVertex
    - A Vertex  corresponds to a PrimaryEvent

```c++
class MCTruthVertex {
    std::string sParticleName;
    std::string sVolumeName;
    TVector3    fPosition_mm;
    Float_t     fEnergyDep_keV;
    Double_t    fTime_ns;
    StepEvent   fStepEvent; // Has all the Step information for tracking, including ParentTracks

};
```


- Electronic Component

```c++
class ElectronicComponent {
    public:
        ElectronicComponent();
        ~ElectronicComponent();
        void doResponse(Pulse& thePulse); // This is the general form that all electronic components will take.
        // they will all have a "doResponse" method that takes as input a pulse. That pulse being

}
```
