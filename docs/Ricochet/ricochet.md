# Ricochet
Ricochet is a future neutrino experiment housed at the H7 site of the Institut Laue-Langevin (ILL) in Grenoble, France.

## Background

### Neutrinos

Neutrinos are at the forefront of scientific curiosity in the fields of particle physics, nuclear physics, and cosmology. Some of the remaining unanswered questions regarding the neutrino are:

- What is the scale and structure of neutrino masses?
-   - Do the masses change? Do they oscillate?
- Are neutrinos their own antiparticle?
- Are there _for sure_ only 3 flavors of neutrinos?

But throughout the past decade, experimental efforts involving a wide array of technologies and techniques bring us to the following knowledge:

1. neutrinos have _a_ mass.

2. these masses have significant mixing with each other.

Expanding our knowledge of this elusive particle has significant ramifications for the Standard Model of particle physics, potentially expanding the SM into an entirely new physics paradigm and how we understand the universe.

#### Neutrino Sources

Neutrinos originate from many sources, but the neutrinos capable of undergoing coherent scattering are produced in huge amounts in the Sun and in nuclear reactors, the latter source being important to the Ricochet experiment.

### Coherent Elastic Neutrino-Nucleus Scattering

This is a physical process whereby an incident neutrino elastically scatters with a nucleus.

!!! info "Coherence Condition"
        When the wavelength of the scattering is longer than the size of the nucleus.

This coherence condition is _guaranteed_ for nearly all nuclear targets when neutrino energies are below $\sim 10 \text{MeV}$.

### Interaction

!! todo "Describe CENNS interaction"
		Include Feynmann diagram too

​



### Related Experiment - COHERENT

The COHERENT experiment reported the first CENNS detection at the 6.7-sigma level[^2], the details of which you can explor further in D. Akimov et al. COHERENT has proven the existence of CENNS detection method, which gives us the green light to explore all kinds of new scientific opportunities.

## The Ricochet Experiment

The Ricochet Experiment consists of a cryogenic bolometer-based detector installation at the H7 site of the Institut Laue-Langevin (ILL) site, home to a high-flux research reactor, perfect for obtaining a CENNS measurement.
What seperates Ricochet from other CENNS experiments will be to aim for a kg-scale experiment with unparalleled background rejection capabilities, down to the 50 eV energy threshold demonstrated by the EDELWEISS Collaboration [@Armengaud_2019]

The background rejection techniques will be based on particle identification and will be achieved thanks to simultaneous heat measurements from the semi-conducting detectors and ionization measurements from the super-conducting detectors.

A dominant background of concern is the electromagnetic background caused by gammas, betas, and low-energy x-rays, which produce [electronic recoils]() in the target medium. However, these electronic recoils can be unambiguously discriminated from the CENNS-induced _nuclear_ recoils.

The ultimate background to overcome are neutrons, since they cannot be discriminated from the CENNS signals, especially if correlated with reactor activity.

The second key feature of our detection approach is to combine two monolithic targets, Germanium and Zinc, as well as cryogenic detector techniques to benefit from a complementary system and reducing systematic error by examining a broader scope of physics interaction. The two cryogenic bolometers being developed are the Germanium CryoCube and the Zinc Q-Array, totally just 1.3 kg. The estimated delivery time will be some time in 2022.

![Experiment Side View](/Images/ExperimentSetup.png){: align=center }

Figure : Side view of the proposed Ricochet experiment to be deployed at the ILL. It stands roughly 8 meters away from the reactor core. The outer structure is comprised of two frames, the integrated shielding frame and the gas

### Detector Technologies

The Ricochet detectors are designed for the first precise CENNS measurement below the 100 eV energy region, specifically searching for new physics in the electroweak sector. In order to accomplish this, the detector must meet the following criterion:

1. Energy thresholds in the $O(10)\text{eV}$ regime.
2. Significant background rejection.
3. Total target mass of about 1 kg.
4. Accomodation of different monolithic target materials, as most new physics depends on the target's nuclear properties.

These can be accomplished with the aforementioned detector technologies, Ge and Zn semi- and super-conductors.

#### Germanium Semi-conductors
Germanium semi-conductors have thusfar been the subject of more study than the zinc counterparts, their fabrication has already been started.

Two key features have to be met in order to achieve the desired rejection of electromagnetic radiation:
1. Fully Inter-Digited (FID) electrodes
   - These allow events happened near the surface to be tagged as such and reject4ed while provided great charge collection for the bulk of other events.
2. ~20eV ionization energy resolution (RMS), which is fives times that of the best resolution achieved so far, however, this may indeed be achievved thanks to dedicated low-noise HEMT-based preamplifiers combined with low-capacitance cabling and detectors.

> Ge prototype image
<!-- ![Ge prototype](/Images/ge-prototype.png){ : style="height:293px;width:386px}

![](/Images/CryogenicDetectorAssembly.png){: style="height:100px;width:100px} -->

>Cryogenic Detector Assembly image

#### Zinc Super-conductors

There are two pieces of motivation for studying the usage of zinc detectors:
1. Zn detectors may uniquely offer strong discrimination capabilities between events arising from residual backgrounds and CENNS-induced events (nuclear recoils)
2. A new detection technique that could reach down to the Cooper pair binding energy.[^1]

In addition, the physical mechanisms of operation are two fold. For one, electromagnetic backgrounds will interact with the detector mediuim predominantly by breaking Cooper pairs and forming quasi-particles, which then recombine and emit _athermal_ phonons. Note, the quasiparticle lifetime increases drastically when they are below their critical temperature (850 mK for Zinc), due to the suppressed phonon-quasiparticle coupling. Because of this, _we expect drastic differences in the thermalization time constants between electronic and nuclear recoils_. This model was first developed by Kaplan, and can be read about further here[@PhysRevB].

Building on this notion, he was able to calculate the time the qwuasiparticles would need to recombine in superconductors at a temperature of 100 mK as _several seconds_. If this turns out to be true, this would mean the bolometers, tuned to have few tens of milliseconds of thermal decay time constants, would _not_ be sensitive to electromagnetic radiation. Such behaviour is very promising for the rigorous background reject capabilities the experiment requires.

The following is the physical Q-Array configuration. Each detector (zinc cube) is fitted with two gold contact pads, one in direct contact with the zinc, the other having a 50-100nm Zinc Oxide (ZnO) layer between the metals. This configuration allows one to simultaneously measure the phonon and the phonon+quasi-particle population from a given energy deposition. This will also allow for futher background discrimination capabilities.

#### Transition Edge Sensors.
For the detector readout, we plan to use transition edge sensors (TES) for the readout of the phonon/quasiparticle signals. TES chips have a unique ability to obtain incredibly low-energy thresholds and background discrimination while only using a single heat channel. This means it is possible to scale up this experimental configuration to build arrays of 100s or 1000s of individual zinc bolometers. However, there is a caveat to this. In order to scale like this, an experimentor must be able to readout each of the individual bolometers _simultaneously_. This can be done by what is called a multiplexed-readout. Multiplexed SQUID arrays are currently an active area of research among the MIT group and other US groups.

#### Multiplexing
This is important because the cryogenic detectors are operating on the milli-Kelvin scale, so we only want 1 wire connecting the inside of the detector to the scorching hot laboratory room.

The dedicated electronic readout will be based on multiplexed SQUID arrays and parametric amplifiers, which will allow hundreds of channels to be readout simultaneously.
A first demonstrator of this new technology, consisting of an array of 3 by 3 40-gram Zinc bolometers, will be integrated together with the CryoCube in the Ricochet cryostat by mid-2022.


### Shielding

The cryostat will be surrounded by layers of passive materials that block outside interference and environmental backgrounds. The preliminary shielding design consists of one layer of borated polyethylene (40cm thick), a muon veto, then a 20cm lead layer, and then an additional 40cm polyethylene layer to futher reduce reactogenic (from a reactor) neutrons.


\bibliography

<!-- [^1] :
    Y. Hochberg, M. Pyle, Y. Zhao and K. M. Zurek, JHEP 1608, 057 (2016)


[^2] :
    D. Akimov et al, “Observation of Coherent Elastic Neutrino Nucleus Scattering”, Science, (2017).


[^3] :
    E. Armengaud et al. \[EDELWEISS Collaboration\], Phys. Rev. D 99, no. 8, 082003 (2019). [arXiv](https://arxiv.org/abs/1901.03588) -->
