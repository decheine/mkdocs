# TODO List

??? info "Organize this doc very nicely"
    Currently it's a bit all over the place, with much documentation still written locally.

??? check "Implement function to save the MCTruth structs in a root file"
    Output format: rderOut.root file containing a `TTree` (?) with branches of each of the MCTruth structs.
    Start by just looking at 3-5 specific `SimuEvent`'s to have processed.
    Each of these SimuEvent's should map to an entry in the

