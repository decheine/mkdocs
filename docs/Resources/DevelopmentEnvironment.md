# Development Environment

---
# ROOT
> Version: 6.22
>
> Documentation: [Building ROOT from source](https://root.cern/install/build_from_source/)


Installing ROOT locally
### Setup
1. Install dependencies

!!! todo "copy from root install page. for now follow from above" link
    Follow along with [this site](https://cloudwafer.com/blog/installing-openssl-on-ubuntu-16-04-18-04/) for configuring openssl with Ubuntu

1. Create a top level directory. in my case `/home/physics/ROOT/`
1. Clone repo

``` bash
git clone --branch v6-22-00-patches https://github.com/root-project/root.git root_src
```
1. Create a build and install directory outside the source directory.

``` bash
mkdir <builddir> <installdir>
cd <builddir>
```

### Build
Execute your desired `cmake` command.


!!! info "ROOT CMake Flags"
    From the `/build` directory:
    ``` bash
    cmake \
    -DCMAKE_INSTALL_PREFIX=../install \
    -DCXX_STANDARD=14 \
    -Dcxx14=ON \
    ../root_src
    ```

??? warning "Ubuntu fix"
    issue and solution:
    libtbb2 libtbb-dev should be updated. see [this](https://github.com/OpenGATE/Gate/issues/254)

### Install

!!! info ""
    ``` bash
    cmake --build . --target install -- -j8
    ```

### Environment

!!! info "Source ROOT environment"
    ``` bash
    source <installdir>/bin/thisroot.sh # or thisroot.{csh,fish,bat} depending on the environment
    ```
To have ROOT setup automatically at each login, that command can be appended to a .profile, .login, .bashrc or equivalent configuration file.

---

# Geant4
> Version: 10.6

> Source: https://geant4.web.cern.ch/support/download_archive

> Docs: https://geant4-userdoc.web.cern.ch/geant4-userdoc/UsersGuides/InstallationGuide/html/installguide.html

### Setup
1. Create a top level directory. in my case `/home/physics/Geant4/`
2. Unpack source package `geant4.10.06.tar.gz`
3. Create a build and install directory outside the source directory.

``` bash
cd /path/to
mkdir geant4.10.06-build
ls
    geant4.10.06  geant4.10.06-build
```

### Build

!!! info "Geant4 CMake Build"
    From the `/build` directory:
    ``` bash
    cmake -DCMAKE_INSTALL_PREFIX=../install\
    -DGEANT4_INSTALL_DATA=ON \
    -DGEANT4_BUILD_CXXSTD=14 \
    ../geant4.10.06.p02/
    ```

### Install

After configuration has successfully run, run from the build directory:

``` bash
make -j8
make install
```

!!! todo "TODO"
    Document CLHEP process.

---

# Ricochetsim
> GitLab: https://git.ipnl.in2p3.fr/ricochet/ricochetsim

### Setup

1. Clone source

2. Add Geant4 and ROOT directories to `CMakeLists.txt` as

```
set(Geant4_DIR ~/physics/geant4/install)
...
set(ROOT_DIR ~/physics/ROOT/install/cmake)
...
```

### Configure

!!! info "ricochetSim CMake Build"
    ```
    cmake -DCMAKE_INSTALL_PREFIX=../install \
    -DCMAKE_PREFIX_PATH=~/physics/geant4/install \
    -DWITH_CRY=OFF \
    ../ricochetsim
    ```
### Build

From the build folder:
``` bash
$ make
```

### Install

From the build folder
``` bash
$ make install
```



### Configuring ricochetSim Libraries

Regarding the alias `src_ricochet`. To configure dictionaries for use, like SimuOutput, need to run

``` bash
$ export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/decheine/physics/ricochet/install/lib
```

This command, and others like it, can be set in your `.bashrc`


!!! warning "LD_LIBRARY_PATH"
    generaly, adding entries to `LD_LIBRARY_PATH` is bad practice, but this works very well. Looking in to the proper way to do this if this is not already that.

!!! info "Info"
    In order to get your IDE to have the correct environment, open a terminal,
    source Geant4, ROOT, AND ricochetSim

    ``` bash
    $ src_geant
    $ src_root
    $ src_ricochet
    ```
