# MkDocs tips for building, developing, and testing

# Run locally

On a windows machine (at least in my case), from the `mkdocs/` directory, run

termblock



### [Admonition](https://squidfunk.github.io/mkdocs-material/extensions/admonition/)

This is great, it allows me to add little blocks of things I find interesting.

* Note
  * with title `!!! note "Title"`
  * without title `!!! note ""`

**Collapsible**

* `!!!` - open by default
* `???` - collapsible


##### Defaults



!!! note "Note Button"
    Can use `note` or `seealso`

!!! abstract "Abstract Button"
    Can use `abstract,summary,tldr`

!!! info "Info Button"
    Can use `info,todo`

!!! tip "Fire Button"
    `tip,hint,important`

!!! success "Checkmark Button"
    `success, check, done`

!!! question "Question Mark"
    `question,help,faq`

!!! warning "Warning Sign"
    `warning,caution,attention`

!!! failure "X Mark"
    `failure,fail,missing`

!!! danger "Danger sign"
    `danger, error`

!!! bug "Bug sign"
    `bug`

!!! example "Numbered List"
    `example`

!!! quote "Quotation Marks"
    `quote, cite`


##### What Now?

!!! question
    How does this work if I want to design my own symbol?

??? todo
    Design my own symbol and add it to the research notebook!

---
