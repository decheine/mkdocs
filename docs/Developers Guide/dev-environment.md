# Notes for setting up the environment

## Required Software
- ROOT v ...
- GEANT4 ..
- ...


### Installing dependencies

### Building ROOT from source

On Ubuntu 20.04 this requires some additional steps that are not detailed by the ROOT website, https://root.cern/install/build_from_source/
Found this guide http://ubuntuhandbook.org/index.php/2020/07/install-qt4-ubuntu-20-04/


### Installing CVMFS / Docker
Following along with [this](https://luxzeplin.gitlab.io/docs/softwaredocs/computing/laptop.html#docker) guide. So far no issue
- After generating an ssh key and adding it to GitLab, `git clone` worked for the dockerscripts, as per the instructions.

on the step
```
source rundocker.sh
```
There are permission errors. setupdocker.sh was able to be run by `sudo ./setupdocker.sh`
To be able to run docker as a non-root user, first have to add $USER to the
docker sudo group,
```
sudo usermod -aG docker $USER
```
Followed along with [this](https://stackoverflow.com/questions/48957195/how-to-fix-docker-got-permission-denied-issue) SO thread

running
```
./rundocker.sh
```
Brings me into the lz environment, but permissions are limited. Directories
cannot be created. I'm assuming I'm using it wrong.



# ROOT

```
sudo apt-get install libjpeg
```

```
cmake \
-DCMAKE_INSTALL_PREFIX=../install \
-DCXX_STANDARD=14 \
-Dcxx14=ON \
../root
```


issue and solution:
libtbb2 libtbb-dev should be updated. see [this](https://github.com/OpenGATE/Gate/issues/254)

weird solution that worked for someone else:


**Built**

maybe needs fewer cores because compiling takes a lot of memory
cmake --build . --target install -- -4

# Development Environment

## GEANT4
In `/build` directory:
```
make -DCMAKE_INSTALL_PREFIX=../install\
-DGEANT4_INSTALL_DATA=ON \
-DGEANT4_BUILD_CXXSTD=14 \
../geant4.10.06.p02/
```
**Built**

### CLHEP
Ricochetsim needs at least **2.3.3.0**. we still want the latest.

For development, if the install data flag isn't working for the cmake project, building from source should.



## Ricochetsim
In a terminal, run
```
source ~/physics/ROOT/install/bin/thisroot.sh
source ~/physics/geant4/install/bin/geant4.sh
```
then from the same terminal, open CLion
```
CLion
```


Add the Geant4 and ROOT dirs to CMakeLists.txt
```
set(Geant4_DIR ~/physics/geant4/install)
...
set(ROOT_DIR ~/physics/ROOT/install/cmake)
...
```

```
cmake -DCMAKE_INSTALL_PREFIX=../install \
-DCMAKE_PREFIX_PATH=~/physics/geant4/install \
-DWITH_CRY=ON \
../ricochetsim
```

In order to get CLion to have the correct environment, you must first enter the terminal, enter

- For the configuration in the IDE, must have the "Before launch" have **build** and **install**



** Tuesday Oct 20 **

## ROOT from command line

For analysis with `ricochetsim` classes, the following are required after first loading root.

```
gSystem->Load("$HOME/physics/ricochet/install/lib/libRicochetSimuOutput.so")
```
